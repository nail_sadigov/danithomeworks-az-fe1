class Hamburger {
    constructor(size, stuffing){
        try{
            if(size === undefined) throw new HamburgerException('invalid size')
            if(stuffing === undefined) throw new HamburgerException('invalid stuffing')
    
            this._size = size
            this._stuffing = stuffing
            this._topping = [];
        }catch(e){
            alert('error ' + e.message);
        }
        }

    set addTopping(topping){
        try{
            if(topping === undefined) throw new HamburgerException('invalid topping for add')
            if(this._topping === undefined) throw new HamburgerException('invalid topping arr')
            if(this._topping.includes(topping)) throw new HamburgerException('There is this topping already')

            this._topping.push(topping)
            console.log(`${topping.name} been added`);
            } catch(e){
                alert('error ' + e.message);
            }
    }

    set removeTopping(topping){
        try{
            if (topping === undefined) throw new HamburgerException('invalid topping for remove')
            if (this.topping === undefined) throw new HamburgerException('invalid topping arr')
            if (!this._topping.includes(topping)) throw new HamburgerException('There is no such a topping')

            this._topping.splice(this._topping.indexOf(topping), 1);
            console.log(`${topping.name} been removed`);
        } catch(e){
            alert('error ' + e.message);
        }
    }

    get topping(){
        return this._topping
    }

    get size(){
        return this._size.name
    }

    get stuffing(){
        return this._stuffing.name
    }

    get callories(){
        let callories = this._size.cal + this._stuffing.cal;
        this._topping.forEach((elem)=>{
        callories += elem.cal
        })
        return callories
    }

    get price(){
        let cost = this._size.cost + this._stuffing.cost;
        this._topping.forEach((elem)=>{
             cost += elem.cost
        })
        return cost
    }

    HamburgerException(message) { 
        this.message = message;
     }
}
 
Hamburger.size = {
    small : {
        type: 'size',
        name: 'small',
        cost : 50,
        cal : 20
    },
    large : {
        type: 'size',
        name: 'large',
        cost : 100,
        cal : 40
    }
}

Hamburger.stuffing = {
    cheese : {
        type: 'stuffing',
        name: 'cheese',
        cost : 10,
        cal : 20
    },
    salad : {
        type: 'stuffing',
        name: 'salad',
        cost : 20,
        cal : 5
    },
    potato : {
        type: 'stuffing',
        name: 'potato',
        cost : 15,
        cal : 10
    }
}

Hamburger.topping = {
    mayo : {
        cost : 20,
        cal : 5,
        name : 'mayo'
    },
    spice : {
        cost : 15,
        cal : 0,
        name : 'spice'
    }
}

 let burger = new Hamburger(Hamburger.size.large, Hamburger.stuffing.cheese)