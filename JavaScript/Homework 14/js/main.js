
$(".content-item:not(':first-of-type')").hide();

$('.tabs-title').on('click', function () {
    $('.tabs-title.active').removeClass('active');
    $(this).addClass('active');
    $('.content-item').hide();
    $('.content-item[data-name ='+ $(this).data('name') +']').show();
});