'use strict';

$(document).ready(function(){
  $(".hide").click(function(){
    $(".most-popular").hide();
  });
  $(".show").click(function(){
    $(".most-popular").show();
  });

$('a[href*="#"]').on('click', function(e) {
  e.preventDefault()

  $('html, body').animate(
    {
      scrollTop: $($(this).attr('href')).offset().top,
    }, 500, 'linear'
  )
});

$(window).scroll(function() {
  if ($(this).scrollTop() >= $(window).height()) {
      $('.back-to-top').fadeIn();
  } else {
      $('.back-to-top').fadeOut();
  }
});

});
  

