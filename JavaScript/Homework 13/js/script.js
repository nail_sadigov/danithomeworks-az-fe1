let style = document.getElementById('theme');

if (localStorage.theme === "alter") {
	style.href = "css/style2.css";
}

let changeStyle = document.getElementById("changeTheme");

changeStyle.onclick = () => {
	if (localStorage.theme === "alter") {
		localStorage.theme = "";
		style.href = "css/style.css";
	}else {
		localStorage.theme = "alter";
		style.href = "css/style2.css";
	}
}